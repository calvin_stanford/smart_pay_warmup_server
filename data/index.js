const port = 5489;
const companies = from('./companies.json');
const project = express();
const express = require('express');
const cors = require('cors');

project.use(cors());
project.get('/companies', (req, res) => res.json(companies))
project.listen(port, () => console.log('SmartPay Warm-up Server is on port '+port +'!'))